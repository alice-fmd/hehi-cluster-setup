#
# Makefile for HEHI Cluster setup packages 
#
SOURCES	:= $(shell find etc -type f -and -not -wholename "*CVS*")	\
	   $(wildcard debian/*prerm)		\
	   $(wildcard debian/*postinst)		\
	   $(wildcard debian/*.menu)		\
	   $(wildcard debian/*.overrides)	\
	   $(wildcard debian/*.install)		\
	   $(wildcard debian/*.links)		\
	   debian/rules				\
	   debian/control			\
	   debian/changelog			\
	   Makefile
PACKAGE := $(shell grep Source debian/control | cut -f2 -d':' | tr -d ' ')
VERSION := $(shell grep $(PACKAGE) debian/changelog | \
	     head -n 1 | cut -f2 -d'(' | cut -f1 -d')')
MAJOR	:= $(basename $(VERSION))
MINOR	:= $(subst .,,$(suffix $(VERSION)))
NEXT	:= $(shell echo $$(($(MINOR)+1)))
DEB	:= $(PACKAGE)_$(VERSION)_all.deb 
ARCH	:= $(shell dpkg-architecture -qDEB_BUILD_ARCH)
CHANGES := $(PACKAGE)_$(VERSION)_$(ARCH).changes
UPLOAD	:= $(PACKAGE)_$(VERSION)_$(ARCH).upload
PUSH	:= $(PACKAGE)_$(VERSION)_$(ARCH).push
HOSTS	:= hehi01 hehi02 hehi03 hehi04
REP	:= /home/hehi/hehi/public_html/ubuntu/dists/hehi/main/binary-$(ARCH)/Packages
PACKS	:= $(shell grep Package: debian/control | cut -f2 -d' ')

all:
	@echo "Nothing to be done"

clean:
	rm -f *~ 
	find etc    -name "*~" | xargs rm -f 
	find debian -name "*~" | xargs rm -f 

install:
	mkdir -p ${DESTDIR}
	cp -a etc ${DESTDIR}/
	cp -a var ${DESTDIR}/
	find ${DESTDIR}/etc -name "CVS" | xargs rm -rf 
	find ${DESTDIR}/var -name "CVS" | xargs rm -rf 

show:
	@echo "PACKAGE		= $(PACKAGE)"
	@echo "VERSION		= $(VERSION)"
	@echo "SOURCES		= "
	@$(foreach i, $(SOURCES), echo "  $(i)" ;)

deb-clean: clean
	fakeroot debian/rules clean
	rm -rf debian/$(PACKAGE) 	\
		debian/*.log		\
		debian/*.substvars	\
		debian/*.debhelper	\
		debian/files		\
		deb.log

../$(DEB) ../$(CHANGES): $(SOURCES)
	$(MAKE) deb-clean
	debuild -us -uc 

../$(UPLOAD):../$(CHANGES)
	dupload -to uhehi00 ../$(CHANGES)

../$(PUSH):../$(UPLOAD) $(REP)
	$(foreach h, $(HOSTS), \
	   ssh -t setup@$(h).hehi.nbi.dk "sudo apt-get update && sudo apt-get -y install $(PACKAGE)" ; )
	touch $@ 

deb-install: deb
	sudo dpkg --purge $(PACKAGE)
	sudo dpkg -i ../$(DEB)

top-clean:
	rm -f ../$(PACKAGE)_*.changes \
	      ../$(PACKAGE)_*.upload \
	      ../$(PACKAGE)_*.push \
	      ../$(PACKAGE)_*.build \
	      ../$(PACKAGE)_*.tar.gz \
	      ../$(PACKAGE)_*.dsc \
	      $(PACKS:%=../%_*.deb) 


upload: ../$(UPLOAD)

deb: ../$(DEB)

push: ../$(PUSH)

version:
	@echo "Current version: $(VERSION), major=$(MAJOR) minor=$(MINOR), next is $(MAJOR).$(NEXT)"
	dch -v $(MAJOR).$(NEXT) -p -M "Updated to version $(MAJOR).$(NEXT)"

#
#
#

